namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contactos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contactos",
                c => new
                    {
                        IdContacto = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 100),
                        SegundoNombre = c.String(maxLength: 100),
                        ApellidoPaterno = c.String(nullable: false, maxLength: 100),
                        ApellidoMaterno = c.String(maxLength: 100),
                        SobreNombre = c.String(maxLength: 100),
                        CorreoElectronico = c.String(maxLength: 255),
                        Movil = c.String(maxLength: 50),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        IdUsuarioCreacion = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdContacto)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUsuarioCreacion)
                .Index(t => t.IdUsuarioCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contactos", "IdUsuarioCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.Contactos", new[] { "IdUsuarioCreacion" });
            DropTable("dbo.Contactos");
        }
    }
}
