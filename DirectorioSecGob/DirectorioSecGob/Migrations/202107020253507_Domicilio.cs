namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Domicilio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DomicilioContactos",
                c => new
                    {
                        IdDomicilio = c.Int(nullable: false, identity: true),
                        IdContacto = c.Int(nullable: false),
                        Calle = c.String(maxLength: 50),
                        Ciudad = c.String(maxLength: 50),
                        Estado = c.String(maxLength: 50),
                        CP = c.Int(),
                        Pais = c.String(maxLength: 50),
                        Telefono = c.String(maxLength: 50),
                        Fax = c.String(maxLength: 50),
                        SitioWeb = c.String(maxLength: 100),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        IdUsuarioCreacion = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdDomicilio)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUsuarioCreacion)
                .ForeignKey("dbo.Contactos", t => t.IdContacto, cascadeDelete: true)
                .Index(t => t.IdContacto)
                .Index(t => t.IdUsuarioCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DomicilioContactos", "IdContacto", "dbo.Contactos");
            DropForeignKey("dbo.DomicilioContactos", "IdUsuarioCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.DomicilioContactos", new[] { "IdUsuarioCreacion" });
            DropIndex("dbo.DomicilioContactos", new[] { "IdContacto" });
            DropTable("dbo.DomicilioContactos");
        }
    }
}
