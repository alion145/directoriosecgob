namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OtrocontactoIdContacto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OtrosContactos", "IdContacto", c => c.Int(nullable: false));
            CreateIndex("dbo.OtrosContactos", "IdContacto");
            AddForeignKey("dbo.OtrosContactos", "IdContacto", "dbo.Contactos", "IdContacto", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OtrosContactos", "IdContacto", "dbo.Contactos");
            DropIndex("dbo.OtrosContactos", new[] { "IdContacto" });
            DropColumn("dbo.OtrosContactos", "IdContacto");
        }
    }
}
