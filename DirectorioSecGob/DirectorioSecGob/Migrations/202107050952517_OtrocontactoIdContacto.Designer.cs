// <auto-generated />
namespace DirectorioSecGob.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class OtrocontactoIdContacto : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(OtrocontactoIdContacto));
        
        string IMigrationMetadata.Id
        {
            get { return "202107050952517_OtrocontactoIdContacto"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
