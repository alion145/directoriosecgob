﻿using DirectorioSecGob.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DirectorioSecGob.Controllers
{
    public class HomeController : Controller
    {
        readonly ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var contactos = db.Contactos.Where(p=>p.Activo == true).ToList();
            ContactosViewModels contactosViewModels = new ContactosViewModels();
            contactos.ForEach(p =>
            {
                var domicilio = db.Domicilio.FirstOrDefault(q => q.IdContacto == p.IdContacto);
                var trabajo = db.Trabajo.FirstOrDefault(q => q.IdContacto == p.IdContacto);
                contactosViewModels.Contactos.Add(new InboxContactosViewModel
                {
                    Contacto = p,
                    Domicilio = domicilio,
                    Trabajo = trabajo
                });
            });
            return View(contactosViewModels);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}