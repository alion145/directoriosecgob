namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActivoContactos3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contactos", "Activo", c => c.Boolean(nullable: false, defaultValue: true));
        }

        public override void Down()
        {
            AlterColumn("dbo.Contactos", "Activo", c => c.Boolean(nullable: false));
        }
    }
}
