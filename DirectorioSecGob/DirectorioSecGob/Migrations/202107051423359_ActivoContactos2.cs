namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActivoContactos2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contactos", "Activo", c => c.Boolean(defaultValue: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contactos", "Activo", c => c.Boolean(nullable: false));
        }
    }
}
