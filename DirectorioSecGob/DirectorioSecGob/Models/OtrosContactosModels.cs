﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DirectorioSecGob.Models
{
    [Table("OtrosContactos")]
    public partial class OtrosContactosModels
    {
        [Key]
        public int IdOtroContacto { get; set; }

        [ForeignKey("Contactos")]
        [Required(ErrorMessage = "Contacto obligatorio.")]
        [Display(Name = "Contacto")]
        public int IdContacto { get; set; }

        [Display(Name = "Contacto")]
        public virtual ContactosModels Contactos { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Nombre obligatorio.")]
        [Display(Name = "Nombre Completo")]
        public string Nombre { get; set; }

        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Correo electrónico no válido.")]
        [Display(Name = "Correo Electrónico")]
        public string CorreoElectronico { get; set; }

        [StringLength(50)]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [StringLength(50)]
        [Display(Name = "Celular")]
        public string Movil { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUsuarioCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class OtrosContactosdbcontex : DbContext
    {
        public DbSet<OtrosContactosModels> OtrosContactos { get; set; }
    }
}