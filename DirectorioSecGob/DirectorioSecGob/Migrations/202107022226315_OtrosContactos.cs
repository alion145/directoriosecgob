namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OtrosContactos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OtrosContactos",
                c => new
                    {
                        IdOtroContacto = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 100),
                        CorreoElectronico = c.String(maxLength: 255),
                        Telefono = c.String(maxLength: 50),
                        Movil = c.String(maxLength: 50),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        IdUsuarioCreacion = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdOtroContacto)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUsuarioCreacion)
                .Index(t => t.IdUsuarioCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OtrosContactos", "IdUsuarioCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.OtrosContactos", new[] { "IdUsuarioCreacion" });
            DropTable("dbo.OtrosContactos");
        }
    }
}
