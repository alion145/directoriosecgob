namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActivoContactos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contactos", "Activo", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contactos", "Activo");
        }
    }
}
