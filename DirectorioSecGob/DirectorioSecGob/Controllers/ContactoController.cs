﻿using DirectorioSecGob.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DirectorioSecGob.Controllers
{
    [Authorize]
    public class ContactoController : Controller
    {
        readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Contacto
        public ActionResult Contacto(int id = 0)
        {
            ContactosViewModels contactosViewModels = new ContactosViewModels();
            if(!id.Equals(0))
            {
                contactosViewModels.Contacto = db.Contactos.FirstOrDefault(p => p.IdContacto == id);
                contactosViewModels.Domicilio = db.Domicilio.FirstOrDefault(p => p.IdContacto == id);
                contactosViewModels.Trabajo = db.Trabajo.FirstOrDefault(p => p.IdContacto == id);
                contactosViewModels.OtroContacto = db.OtrosContactos.FirstOrDefault(p => p.IdContacto == id);
            }
            return View(contactosViewModels);
        }

        public bool CambiosContacto(ContactosModels contacto)
        {
            ApplicationDbContext db2 = new ApplicationDbContext();
            ContactosModels contactoBD = db2.Contactos.FirstOrDefault(p => p.IdContacto == contacto.IdContacto);
            if(contacto.Nombre != contactoBD.Nombre)
            {
                return true;
            }
            if (contacto.SegundoNombre != contactoBD.SegundoNombre)
            {
                return true;
            }
            if (contacto.ApellidoPaterno != contactoBD.ApellidoPaterno)
            {
                return true;
            }
            if (contacto.ApellidoMaterno != contactoBD.ApellidoMaterno)
            {
                return true;
            }
            if (contacto.SobreNombre != contactoBD.SobreNombre)
            {
                return true;
            }
            if (contacto.CorreoElectronico != contactoBD.CorreoElectronico)
            {
                return true;
            }
            if (contacto.Movil != contactoBD.Movil)
            {
                return true;
            }

            return false;
        }

        public bool CambiosDomicilio(DomicilioModels domicilio)
        {
            ApplicationDbContext db2 = new ApplicationDbContext();
            DomicilioModels domicilioBD = db2.Domicilio.FirstOrDefault(p => p.IdDomicilio == domicilio.IdDomicilio);
            if (domicilio.Calle != domicilioBD.Calle)
            {
                return true;
            }
            if (domicilio.Ciudad != domicilioBD.Ciudad)
            {
                return true;
            }
            if (domicilio.Estado != domicilioBD.Estado)
            {
                return true;
            }
            if (domicilio.CP != domicilioBD.CP)
            {
                return true;
            }
            if (domicilio.Pais != domicilioBD.Pais)
            {
                return true;
            }
            if (domicilio.Telefono != domicilioBD.Telefono)
            {
                return true;
            }
            if (domicilio.Fax != domicilioBD.Fax)
            {
                return true;
            }
            if (domicilio.SitioWeb != domicilioBD.SitioWeb)
            {
                return true;
            }

            return false;
        }

        public bool CambiosTrabajo(TrabajoModels trabajo)
        {
            ApplicationDbContext db2 = new ApplicationDbContext();
            TrabajoModels trabajoBD = db2.Trabajo.FirstOrDefault(p => p.IdTrabajo == trabajo.IdTrabajo);
            if (trabajo.Calle != trabajoBD.Calle)
            {
                return true;
            }
            if (trabajo.Ciudad != trabajoBD.Ciudad)
            {
                return true;
            }
            if (trabajo.Estado != trabajoBD.Estado)
            {
                return true;
            }
            if (trabajo.CP != trabajoBD.CP)
            {
                return true;
            }
            if (trabajo.Pais != trabajoBD.Pais)
            {
                return true;
            }
            if (trabajo.Telefono != trabajoBD.Telefono)
            {
                return true;
            }
            if (trabajo.Fax != trabajoBD.Fax)
            {
                return true;
            }
            if (trabajo.SitioWeb != trabajoBD.SitioWeb)
            {
                return true;
            }
            if (trabajo.Organizacion != trabajoBD.Organizacion)
            {
                return true;
            }
            if (trabajo.Puesto != trabajoBD.Puesto)
            {
                return true;
            }
            if (trabajo.Departamento != trabajoBD.Departamento)
            {
                return true;
            }
            if (trabajo.Oficina != trabajoBD.Oficina)
            {
                return true;
            }

            return false;
        }

        public bool CambiosOtroContacto(OtrosContactosModels otroContacto)
        {
            ApplicationDbContext db2 = new ApplicationDbContext();
            OtrosContactosModels otroContactoBD = db2.OtrosContactos.FirstOrDefault(p => p.IdOtroContacto == otroContacto.IdOtroContacto);
            if (otroContacto.Nombre != otroContactoBD.Nombre)
            {
                return true;
            }
            if (otroContacto.CorreoElectronico != otroContactoBD.CorreoElectronico)
            {
                return true;
            }
            if (otroContacto.Telefono != otroContactoBD.Telefono)
            {
                return true;
            }
            if (otroContacto.Movil != otroContactoBD.Movil)
            {
                return true;
            }

            return false;
        }

        [HttpPost]
        public ActionResult Guardar(ContactosViewModels contactosViewModels)
        {
            try
            {
                if (ModelState.ContainsKey("Contacto.IdContacto"))
                    ModelState["Contacto.IdContacto"].Errors.Clear();
                if (ModelState.ContainsKey("Domicilio.IdDomicilio"))
                    ModelState["Domicilio.IdDomicilio"].Errors.Clear();
                if (ModelState.ContainsKey("Domicilio.CP"))
                    ModelState["Domicilio.CP"].Errors.Clear();
                if (ModelState.ContainsKey("Trabajo.IdTrabajo"))
                    ModelState["Trabajo.IdTrabajo"].Errors.Clear();
                if (ModelState.ContainsKey("Trabajo.CP"))
                    ModelState["Trabajo.CP"].Errors.Clear();
                if (ModelState.ContainsKey("OtroContacto.IdOtroContacto"))
                    ModelState["OtroContacto.IdOtroContacto"].Errors.Clear();
                if (ModelState.ContainsKey("OtroContacto.Nombre"))
                    ModelState["OtroContacto.Nombre"].Errors.Clear();
                if (!ModelState.IsValid)
                {
                    return View("Contacto", contactosViewModels);
                }

                if (contactosViewModels.Contacto == null)
                {
                    throw new Exception("Debe llenar la información de contacto.");
                }else
                {
                    contactosViewModels.Contacto.Activo = true;
                }

                if (contactosViewModels.Contacto.IdContacto.Equals(0))
                {
                    contactosViewModels.Contacto.IdUsuarioCreacion = User.Identity.GetUserId();
                    db.Contactos.Add(contactosViewModels.Contacto);
                }
                else
                {
                    if (this.CambiosContacto(contactosViewModels.Contacto))
                    {                        
                        db.Entry(contactosViewModels.Contacto).State = EntityState.Modified;
                        var historial = new HistorialModels
                        {
                            IdContacto = contactosViewModels.Contacto.IdContacto,
                            Informacion = "Se modificó información de contacto",
                            IdUsuarioCreacion = User.Identity.GetUserId()
                        };
                        db.Historial.Add(historial);
                    }
                }
                
                if (contactosViewModels.Domicilio != null)
                {
                    contactosViewModels.Domicilio.IdContacto = contactosViewModels.Contacto.IdContacto;
                    if (contactosViewModels.Domicilio.IdDomicilio.Equals(0))
                    {
                        if (contactosViewModels.Domicilio.Calle != null || contactosViewModels.Domicilio.Ciudad != null || contactosViewModels.Domicilio.Estado != null || !contactosViewModels.Domicilio.CP.Equals(0) || contactosViewModels.Domicilio.Pais != null || contactosViewModels.Domicilio.Telefono != null || contactosViewModels.Domicilio.Fax != null || contactosViewModels.Domicilio.SitioWeb != null)
                        {
                            contactosViewModels.Domicilio.IdUsuarioCreacion = User.Identity.GetUserId();
                            db.Domicilio.Add(contactosViewModels.Domicilio);
                        }
                    }
                    else
                    {
                        if(this.CambiosDomicilio(contactosViewModels.Domicilio))
                        {
                            db.Entry(contactosViewModels.Domicilio).State = EntityState.Modified;
                            var historial = new HistorialModels
                            {
                                IdContacto = contactosViewModels.Contacto.IdContacto,
                                Informacion = "Se modificó información de domicilio",
                                IdUsuarioCreacion = User.Identity.GetUserId()
                            };
                            db.Historial.Add(historial);
                        }
                    }
                }

                if (contactosViewModels.Trabajo != null)
                {
                    contactosViewModels.Trabajo.IdContacto = contactosViewModels.Contacto.IdContacto;
                    if (contactosViewModels.Trabajo.IdTrabajo.Equals(0))
                    {
                        if (contactosViewModels.Trabajo.Calle != null || contactosViewModels.Trabajo.Ciudad != null || contactosViewModels.Trabajo.Estado != null || !contactosViewModels.Trabajo.CP.Equals(0) || contactosViewModels.Trabajo.Pais != null || contactosViewModels.Trabajo.Telefono != null || contactosViewModels.Trabajo.Fax != null || contactosViewModels.Trabajo.SitioWeb != null || contactosViewModels.Trabajo.Organizacion != null || contactosViewModels.Trabajo.Puesto != null || contactosViewModels.Trabajo.Departamento != null || contactosViewModels.Trabajo.Oficina != null)
                        {
                            contactosViewModels.Trabajo.IdUsuarioCreacion = User.Identity.GetUserId();
                            db.Trabajo.Add(contactosViewModels.Trabajo);
                        }
                    }
                    else
                    {
                        if (this.CambiosTrabajo(contactosViewModels.Trabajo))
                        {
                            db.Entry(contactosViewModels.Trabajo).State = EntityState.Modified;
                            var historial = new HistorialModels
                            {
                                IdContacto = contactosViewModels.Contacto.IdContacto,
                                Informacion = "Se modificó información de trabajo",
                                IdUsuarioCreacion = User.Identity.GetUserId()
                            };
                            db.Historial.Add(historial);
                        }
                    }
                }

                if (contactosViewModels.OtroContacto != null)
                {
                    contactosViewModels.OtroContacto.IdContacto = contactosViewModels.Contacto.IdContacto;
                    if (contactosViewModels.OtroContacto.IdOtroContacto.Equals(0))
                    {
                        if (contactosViewModels.OtroContacto.Nombre != null || contactosViewModels.OtroContacto.CorreoElectronico != null || contactosViewModels.OtroContacto.Telefono != null || contactosViewModels.OtroContacto.Movil != null)
                        {
                            contactosViewModels.OtroContacto.IdUsuarioCreacion = User.Identity.GetUserId();
                            db.OtrosContactos.Add(contactosViewModels.OtroContacto);
                        }
                    }
                    else
                    {
                        if (this.CambiosOtroContacto(contactosViewModels.OtroContacto))
                        {
                            db.Entry(contactosViewModels.OtroContacto).State = EntityState.Modified;
                            var historial = new HistorialModels
                            {
                                IdContacto = contactosViewModels.Contacto.IdContacto,
                                Informacion = "Se modificó información de otro contacto",
                                IdUsuarioCreacion = User.Identity.GetUserId()
                            };
                            db.Historial.Add(historial);
                        }
                    }
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            return RedirectToAction("Index", "Home");
        }
        
        public ActionResult Eliminar(int id = 0)
        {
            using (db)
            {
                ContactosModels contacto = db.Contactos.FirstOrDefault(p => p.IdContacto == id);
                contacto.Activo = false;
                db.Entry(contacto).State = EntityState.Modified;
                var historial = new HistorialModels
                {
                    IdContacto = id,
                    Informacion = "Se desactivó contacto",
                    IdUsuarioCreacion = User.Identity.GetUserId()
                };
                db.Historial.Add(historial);
                db.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}