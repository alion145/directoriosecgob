﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DirectorioSecGob.Models
{
    [Table("Historial")]
    public partial class HistorialModels
    {
        [Key]
        public int IdHistorial { get; set; }

        [ForeignKey("Contactos")]
        [Required(ErrorMessage = "Contacto obligatorio.")]
        [Display(Name = "Contacto")]
        public int IdContacto { get; set; }

        [Display(Name = "Contacto")]
        public virtual ContactosModels Contactos { get; set; }

        [StringLength(200)]
        [Display(Name = "Información")]
        public string Informacion { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUsuarioCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Historialdbcontex : DbContext
    {
        public DbSet<HistorialModels> Historial { get; set; }
    }
}