namespace DirectorioSecGob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Historial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Historial",
                c => new
                    {
                        IdHistorial = c.Int(nullable: false, identity: true),
                        IdContacto = c.Int(nullable: false),
                        Informacion = c.String(nullable: false, maxLength: 200),
                        FechaCreacion = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        IdUsuarioCreacion = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdHistorial)
                .ForeignKey("dbo.AspNetUsers", t => t.IdUsuarioCreacion)
                .ForeignKey("dbo.Contactos", t => t.IdContacto, cascadeDelete: true)
                .Index(t => t.IdContacto)
                .Index(t => t.IdUsuarioCreacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Historial", "IdContacto", "dbo.Contactos");
            DropForeignKey("dbo.Historial", "IdUsuarioCreacion", "dbo.AspNetUsers");
            DropIndex("dbo.Historial", new[] { "IdUsuarioCreacion" });
            DropIndex("dbo.Historial", new[] { "IdContacto" });
            DropTable("dbo.Historial");
        }
    }
}
