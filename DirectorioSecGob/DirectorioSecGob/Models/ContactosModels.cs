﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DirectorioSecGob.Models
{
    [Table("Contactos")]
    public partial class ContactosModels
    {
        [Key]
        public int IdContacto { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Nombre obligatorio.")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [StringLength(100)]
        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Apellido paterno obligatorio.")]
        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        [StringLength(100)]
        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }

        [StringLength(100)]
        [Display(Name = "Apodo")]
        public string SobreNombre { get; set; }

        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Correo electrónico no válido.")]
        [Display(Name = "Correo Electrónico")]
        public string CorreoElectronico { get; set; }

        [StringLength(50)]
        [Display(Name = "Celular")]
        public string Movil { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUsuarioCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public bool? Activo { get; set; }
    }

    public class Contactosdbcontex : DbContext
    {
        public DbSet<ContactosModels> Contactos { get; set; }
    }
}