﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DirectorioSecGob.Models
{
    public class ContactosViewModels
    {
        public ContactosViewModels()
        {
            Contacto = new ContactosModels();
            Domicilio = new DomicilioModels();
            Trabajo = new TrabajoModels();
            OtroContacto = new OtrosContactosModels();
            Historial = new HistorialModels();
            Actualizaciones = new List<HistorialModels>();
            Contactos = new List<InboxContactosViewModel>();
        }
        public ContactosModels Contacto { get; set; }
        public DomicilioModels Domicilio { get; set; }
        public TrabajoModels Trabajo { get; set; }
        public OtrosContactosModels OtroContacto { get; set; }
        public HistorialModels Historial { get; set; }
        public IList<HistorialModels> Actualizaciones { get; set; }
        public IList<InboxContactosViewModel> Contactos { get; set; }
    }

    public class InboxContactosViewModel
    {
        public ContactosModels Contacto { get; set; }
        public DomicilioModels Domicilio { get; set; }
        public TrabajoModels Trabajo { get; set; }
        public OtrosContactosModels OtroContacto { get; set; }
    }
}