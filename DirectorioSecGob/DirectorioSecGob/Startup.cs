﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DirectorioSecGob.Startup))]
namespace DirectorioSecGob
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
