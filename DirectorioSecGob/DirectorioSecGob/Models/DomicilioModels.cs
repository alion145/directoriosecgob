﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DirectorioSecGob.Models
{
    [Table("DomicilioContactos")]
    public partial class DomicilioModels
    {
        [Key]
        public int IdDomicilio { get; set; }

        [ForeignKey("Contactos")]
        [Required(ErrorMessage = "Contacto obligatorio.")]
        [Display(Name = "Contacto")]
        public int IdContacto { get; set; }

        [Display(Name = "Contacto")]
        public virtual ContactosModels Contactos { get; set; }

        [StringLength(50)]
        public string Calle { get; set; }

        [StringLength(50)]
        public string Ciudad { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "El valor {0} no es válido para código postal.")]
        [Display(Name = "Código Postal")]
        public int CP { get; set; }

        [StringLength(50)]
        [Display(Name = "País")]
        public string Pais { get; set; }

        [StringLength(50)]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(100)]
        [Display(Name = "Sitio Web")]
        public string SitioWeb { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? FechaCreacion { get; set; }

        [ForeignKey("ApplicationUser")]
        public string IdUsuarioCreacion { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Domiciliodbcontex : DbContext
    {
        public DbSet<DomicilioModels> Domicilio { get; set; }
    }
}